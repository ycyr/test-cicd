terraform {
    required_version = ">= 1.3"
    required_providers {
        local = {
            source = "hashicorp/local"
            version = "~> 2.0"
        }
    }
}

resource "local_file" "test_file" {
    filename = "test2.txt"
    content = <<-EOT
    Bonjour test2
    EOT
}
